// routes/groups.js
const express = require('express');
const router = express.Router();
const Group = require('../models/group');

// Create
router.post('/create', async (req, res) => {
    try {
        const group = await Group.create(req.body);
        res.json(group);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});

// Read
router.get('/:id', async (req, res) => {
    try {
        const group = await Group.findById(req.params.id)
            .populate('members admins', 'name email'); // Inclure seulement les champs nécessaires
        res.json(group);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});

// Update
router.put('/:id', async (req, res) => {
    try {
        const group = await Group.findByIdAndUpdate(req.params.id, req.body, { new: true });
        res.json(group);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});

// Delete
router.delete('/:id', async (req, res) => {
    try {
        await Group.findByIdAndDelete(req.params.id);
        res.json({ message: 'Groupe supprimé avec succès.' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});

module.exports = router;
