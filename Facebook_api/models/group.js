// models/group.js
const mongoose = require('mongoose');

const groupSchema = new mongoose.Schema({
    name: String,
    description: String,
    icon: String,
    coverPhoto: String,
    type: { type: String, enum: ['public', 'private', 'secret'] },
    allowMembersPost: Boolean,
    allowMembersCreateEvents: Boolean,
    members: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],  // Référence aux membres
    admins: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],   // Référence aux administrateurs
});

module.exports = mongoose.model('Group', groupSchema);
