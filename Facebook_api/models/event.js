// models/event.js
const mongoose = require('mongoose');

const eventSchema = new mongoose.Schema({
    name: String,
    description: String,
    startDate: Date,
    endDate: Date,
    location: String,
    coverPhoto: String,
    private: Boolean,
    organizers: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],  // Référence aux utilisateurs (organisateurs)
    members: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],     // Référence aux utilisateurs (membres)
});

module.exports = mongoose.model('Event', eventSchema);
