// index.js
const express = require('express');
const mongoose = require('mongoose');
const app = express();
const port = process.env.PORT || 3000;
const userRoutes = require('./routes/users');
const eventRoutes = require('./routes/events');
const groupRoutes = require('./routes/groups');
const discussionRoutes = require('./routes/discussions');
const albumRoutes = require('./routes/photoAlbums');


const mongoURI = 'mongodb+srv://cicnaq:pwd1234@cluster0.igsr4uu.mongodb.net/Facebook_API'


mongoose.connect(mongoURI, { useNewUrlParser: true, useUnifiedTopology: true });

const db = mongoose.connection;

app.use(express.json());

db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', () => {
  console.log('Connected to MongoDB');

  // Define your Mongoose schema and models here, if needed

  // Start your Express app after establishing the MongoDB connection
  app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
  });
});

process.on('SIGINT', () => {
    mongoose.connection.close(() => {
      console.log('MongoDB connection closed');
      process.exit(0);
    });
  });

app.use('/users', userRoutes);
app.use('/events', eventRoutes);
app.use('/groups', groupRoutes);
app.use('/discussions', discussionRoutes);
app.use('/photoAlbums', albumRoutes);